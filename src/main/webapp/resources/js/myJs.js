/**
 * Created by Marolok on 15.04.2015.
 */

function update_user(pars){
    var mess = "";
    var err = 0;
    // Обязательны к заполнению
    var login = document.getElementById('login').value;
    var pass = document.getElementById('pass').value;
    if (login.length == 0){
        mess += "LOGIN";
        err = 1;
    }
    if (pass.length == 0){
        mess += "PASS";
        err = 1;
    }
    // Обязательно или телефон, или почта
    var tel = document.getElementById('tel').value;
    var email = document.getElementById('email').value;
    if(tel.length == 0 && email.length == 0){
        mess += "Telefon or Email";
        err = 1;
    }
    //Все сразу
    var num = document.getElementById('num').value;
    var ser = document.getElementById('ser').value;
    var date = document.getElementById('date').value;
    var text = document.getElementById('text').value;
    if ( !(num.length != 0 && ser.length != 0 && date.length != 0 && text.length != 0))
    {
        mess += "PASPORT";
        err = 1;
    }
    // Вывод сообщения
    if(err == 1){
        //confirm("Заполните поля:\n"+mess);
        alert(mess);
        return false;
    }
    alert("GOOD");
    pars.submit();
}